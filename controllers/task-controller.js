const app = require('express');
const router = app.Router();
const Task = require('../models/task');

/* Add task api */
router.post('/add', async (req, res, next) => {
    try {
        let {
            title,
            due_date
        } = req.body;
        /* Checking required data. */
        if (!title) {        
            return res.status(402).json({
                status: false,
                message: `Title is required.`
            });
        };
        /* Getting task from db. */
        let taskFound = await Task.findOne({
            title: title
        });
        /* In case of task already exist. */
        if(taskFound){
            return res.status(401).json({
                status: false,
                message: `Task already exist with '${title}' title.`,
                data: null
            });
        }
        if(due_date){
            due_date = new Date(due_date);
            if(due_date == "Invalid Date"){
                return res.status(402).json({
                    status: false,
                    message: `Due date is invalid.`
                });
            };
        };
        newTask = new Task({
            title: title,
            due_date: due_date || undefined
        });
        let taskAdded = await newTask.save();
        if (taskAdded) {
            return res.status(200).json({
                status: true,
                message: `Task added successfully.`,
                data: taskAdded
            });
        };
    } catch (error) {
        return res.status(502).json({
            status: false,
            message: `Something went wrong.`,
            error: error.message
        });
    };       
});

/* get task by title. */
router.get('/', async (req, res, next) => {
    try {
        let {title} = req.query;
        /* Checking required data. */
        if (!title) {
            return res.status(402).json({
                status: false,
                message: `Task title is required.`
            });
        };
        /* Getting task from db. */
        let taskFound = await Task.findOne({
            title: title
        });
        /* In case of task not found. */
        if(!taskFound){
            return res.status(401).json({
                status: false,
                message: `Task not found with '${title}' title.`,
                data: null
            });
        }else{
            /* returning response. */
            return res.status(200).json({
                status: true,
                message: `success`,
                data: taskFound
            });
        };
    }catch(error){
        /* In case of error */
        res.status(501).json({
            status: false,
            message: `Something went wrong.`,
            error: error.message
        });
    };

});

/* Update Task by title. */
router.put('/', async (req, res, next) => {
    try {
        let {
            title,
            due_date
        } = req.body;               
        /* Checking required data. */
        if (!title) {
            return res.status(402).json({
                status: false,
                message: `Task title is required.`
            });
        };
        if(due_date){
            due_date = new Date(due_date);
            if(due_date == "Invalid Date"){
                return res.status(402).json({
                    status: false,
                    message: `Due date is invalid.`
                });
            };
        };
        /* Getting task from db. */
        let taskFound = await Task.findOne({
            title: title
        });
        /* In case of task not found. */
        if(!taskFound){
            return res.status(401).json({
                status: false,
                message: `Task not found with '${title}' title.`,
                data: null
            });
        }else{
            /* updating task. */
            taskFound.due_date = due_date || undefined;
            let taskUpdated = await taskFound.save();
            if (taskUpdated) {
                return res.status(200).json({
                    status: true,
                    message: `Task updated successfully.`,
                    data: taskUpdated
                });
            };
        }
    }catch(error){
        /* In case of error */
        res.status(501).json({
            status: false,
            message: `Something went wrong.`,
            error: error.message
        });
    };
});

/* Delete task API. */
router.delete('/', async (req, res, next) => {
    try {
        let {
            title
        } = req.query;
        /* Checking required data. */
        if (!title) {
            return res.status(402).json({
                status: false,
                message: `Task title is required.`
            });
        };
        /* Getting task from db. */
        let taskFound = await Task.findOne({
            title: title
        });
        /* In case of task not found. */
        if (!taskFound) {
            return res.status(401).json({
                status: false,
                message: `Task not found with '${title}' title.`,
                data: null
            });
        } else {
            /* deleting task. */
            let taskDeleted = await Task.deleteOne({
                title: title
            });
            if (taskDeleted) {
                return res.status(200).json({
                    status: true,
                    message: `Task deleted successfully.`,
                    data: taskDeleted
                });
            };
        }
    } catch (error) {
        /* In case of error */
        res.status(501).json({
            status: false,
            message: `Something went wrong.`,
            error: error.message
        });
    };
});

module.exports = router;