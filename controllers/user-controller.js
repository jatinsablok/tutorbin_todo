const app = require('express');
const router = app.Router();
const User = require('../models/user');
const jwt = require('jsonwebtoken');

/* Register API */
router.post(`/register`, async (req, res, next) => {
    try {
        let {
            email,
            name,
            password
        } = req.body;
        /* Checking required data. */
        if (!email || !name || !password) {
            return res.status(402).json({
                status: false,
                message: `Please send email/name/password in request.`
            });
        };
        /* Checking email is already exist or not. */
        let ifUserExist = await User.findOne({
            email: email
        });
        /* In case of user already exist. */
        if (ifUserExist) {
            return res.status(402).json({
                status: false,
                message: `User with email ${email} already exist.`
            });
        }
        /* Creating new user. */
        let newUser = new User({
            email: email,
            name: name,
            password: password
        });
        /* Saving int database. */
        let userRegistered = await newUser.save();
        if (userRegistered) {
            userRegistered.password = undefined;
            return res.status(200).json({
                status: true,
                message: `User registered successfully.`,
                data: userRegistered
            });
        };
    } catch (error) {
        return res.status(502).json({
            status: false,
            message: `Something went wrong.`,
            error: error.message
        });
    }
});

/* Login API */
router.post(`/login`, async (req, res, next) => {
    try {
        const {
            email,
            password
        } = req.body;
        /* Checking required data. */
        if (!email || !password) {
            return res.status(402).json({
                status: false,
                message: `Please send email/password in request.`
            });
        }
        userExist = await User.findOne({email: email});
        /* In case of user not exist. */
        if (!userExist) {
            return res.status(401).json({
                status: false,
                message: `User with email ${email} not exist.`
            });
        };
        /* In case of user exist. */
        /* Checking password */
        if(password !== userExist.password){
            return res.status(401).json({
                status: false,
                message: `Password is incorrect.`
            });
        };
        /*  */
        /* In case of password is correct. */
        userExist.password = undefined;/* deleting password from the user object */
        return res.status(200).json({
            status: true,
            message: `User logged in successfully.`,
            data: userExist,
            token: jwt.sign({secret: "todo-app-secret-key" }, 'RESTFULAPIs')            
        });

    } catch (error) {
        return res.status(502).json({
            status: false,
            message: `Something went wrong.`,
            error: error.message
        });
    }
});


module.exports = router;