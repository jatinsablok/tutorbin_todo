const mongoose = require('mongoose');

// connect to the database [I used mongodb cloud so you can test my assignment.]
mongoose.connect('mongodb+srv://jatin:jatinsablok@todo-app.tpq6pm8.mongodb.net/?retryWrites=true&w=majority');
const db = mongoose.connection;

// printing error if connection fails.
db.on('error', console.error.bind(console, "Error in connecting to TODO Database."));

// printing success message if connection is successful.
db.once('open', function(){
    console.log('Connected to TODO Database.');
});

module.exports = db;