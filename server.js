/* All credential should be kept in .env file but, I have added openly so you can test my assignment smoothly. */
const express = require("express");
const app = express();
/* Requiring Database connection file for db connection */
require(`./db-connection`);
const port = 9000;
/* Importing authentication middleware */
const auth = require(`./middleware/auth`);
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Importing User controller. */
const UserController = require("./controllers/user-controller");
/* Importing Task controller. */
const TaskController = require("./controllers/task-controller");

/* Routes */
app.use('/user', UserController);
app.use('/task', auth, TaskController);





app.listen(port,function(){
    console.log(`TODO App Started (port: ${port}).`)
})