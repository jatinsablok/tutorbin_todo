const mongoose = require('mongoose');
/* Task Schema */
const taskSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    due_date: {
        type: Date
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});


const Task = mongoose.model('Task', taskSchema);

module.exports = Task;