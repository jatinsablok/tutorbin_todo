const jwt = require('jsonwebtoken');

module.exports = function(req, res, next){
    if(!req.headers.token){
        return res.status(401).json({
            status: false,
            message: `Please send token in header.`
        });
    };
    jwt.verify(req.headers.token, 'RESTFULAPIs',(err, isValid)=>{
        if(err){
            return res.status(401).json({
                status: false,
                message: `Not Authorised`
            });
        }else{
            next();
        }
    });
};